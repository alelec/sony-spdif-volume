from pathlib import Path
from binascii import hexlify

def enable(hiddev):
    print(f"Configuring {hiddev}")
    spdif_loopback_enable = b'\x00\x20\x01\xb0\x01'
    #current = hiddev.read_bytes()
    #print(hexlify(current))
    hiddev.write_bytes(spdif_loopback_enable)

def enable_all():
    for dev in Path("/sys/class/hidraw").iterdir():
        for namef in dev.glob("device/input/*/name"):
            name = namef.read_text()
            if "USB Sound Device" in name:
                devpath = Path("/dev") / dev.name
                enable(devpath)
            
if __name__ == "__main__":
    enable_all()
