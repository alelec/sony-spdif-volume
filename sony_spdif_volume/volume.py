import alsaaudio
import json
# import aiohttp
import asyncio
import httpx
import logging
from logging.handlers import RotatingFileHandler

import sys
from pathlib import Path
mod = Path(__file__).parent
sys.path.append(str(mod / "remotecontrol"))

from sonybraviatv_remotecontrol import simpleipprotocol

import sony_spdif_volume.usb_settings

sony_spdif_volume.usb_settings.enable_all()

MULTIPLIER = 2.5
OFFSET = 3

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] %(message)s")
log = logging.getLogger()
log.setLevel(logging.INFO)

fileHandler = RotatingFileHandler("volume.log", maxBytes=20*1024,
                                  backupCount=5)
fileHandler.setFormatter(logFormatter)
log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)

logging.getLogger("httpx").setLevel("ERROR")
logging.getLogger("httpcore").setLevel("ERROR")

# print(alsaaudio.mixers()[0])
try:
    mixer = alsaaudio.Mixer("Speaker")
except Exception as ex:
    log.exception(ex)
    mixers = alsaaudio.mixers()
    log.info(mixers)
    log.info(f"Using: {mixers[0]}")
    mixer = alsaaudio.Mixer(mixers[0])
current_volume = mixer.getvolume() # Get the current Volume


def set_volume(level):
    global mixer, current_volume
    level = min(100, max(0, int((level-OFFSET) * MULTIPLIER)))
    check = current_volume
    if isinstance(current_volume, list):
        check = current_volume[0]
    if level != check:
        mixer.setvolume(int(level)) # Set the volume to new level.
        log.info(f"Volume: {current_volume} -> {level}")
        current_volume = mixer.getvolume()



def handler(msg_type, cmd, params):
    if cmd == b"VOLU":
        log.info(f"* {cmd} {params}")


async def run_sip():
    while True:
      try:
          conn = simpleipprotocol.SimpleIpProtocol(host, port, handler)
          running = await conn.start_client(timeout=0 if fail_allowed else 60)
          await running
      except OSError:
        if not fail_allowed:
            raise
        await asyncio.sleep(1)
        
async def run(host, port, handler, fail_allowed=False):
    try:
        conn = simpleipprotocol.SimpleIpProtocol(host, port, handler)
        running = await conn.start_client(timeout=0 if fail_allowed else 60)
        log.info(f"Connected on {host}:{port}")
        did = 1
        async with httpx.AsyncClient() as session:
        # async with aiohttp.ClientSession() as session:
            while True:
                await asyncio.sleep(0.25)
                did = ((did + 1) % 256) or 1
                data = {
                    "method": "getVolumeInformation",
                    "id": did,
                    "params": [],
                    "version": "1.0"
                }
                resp = await session.post(f'http://{host}/sony/audio', json=data)
                if resp:
                    #print(resp.status)
                    try:
                        rj = resp.json()
                        if "error" in rj:
                            await asyncio.sleep(1)
                            if "Display Is Turned off" in str(rj):
                                continue
                            log.error(rj)
                            if "Illegal JSON" in str(rj):
                                log.error(json.dumps(data))
                            continue
                        res = rj.get("result")[0][0]
                        # {'result': [[{'target': 'headphone', 'volume': 24
                        # , 'mute': False, 'maxVolume': 100, 'minVolume': 0
                        # }]], 'id': 225}
                        # print("volume", res["volume"])
                        volume = res["volume"]
                        # print("mute", res["mute"])
                        if res["mute"]:
                            volume = 0
                        set_volume(volume)
                    except Exception as ex:
                        log.exception(ex)
                        log.info(rj)
                    await resp.aclose()
                
    except OSError:
        if not fail_allowed:
            raise
        await asyncio.sleep(1)

async def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", default="192.168.0.0")
    parser.add_argument("--port", default=20060)
    args = parser.parse_args()

    host = args.host
    if host.endswith(".0"):
        while True:
            # try to talk to everything from .1 to .254
            await asyncio.gather(*[run(f"{host[:-1]}{ip}", args.port, handler, True) for ip in range(1, 255)])
            
    else:
        await run(host, args.port, handler)

    log.info("Exited")

if __name__ == "__main__":
    while True:
        try:
            asyncio.run(main())
        except Exception as ex:
            log.exception(ex)
