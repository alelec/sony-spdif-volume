# Sony SPDIF Volume
Adjuat the volume of digital audio out from Sony TV to match volume
of built-in TV speakers.

## Requirwmwnts
```
sudo apt-get install -y python3-alsaaudio

echo 'KERNEL=="hidraw*", ATTRS{busnum}=="1", ATTRS{idVendor}=="0d8c", ATTRS{idProduct}=="0102", MODE="0666"' | sudo tee /etc/udev/rules.d/99-hidraw-permissions.rules

```

usb spdif mix
https://unix.stackexchange.com/questions/677207/no-sound-on-iec958-s-pdif-input-sound-card-usb-cm106



perl -e 'print pack "H*", "002001b001"' >/dev/hidraw1
This sets the register to the power-on default value (00b0) + SPDIFMIX enabled (01b0) (in little-endian).

The following shell skript hack sets the bit on every "USB Sound Device" - it might be dangerous when used with the wrong card:
```
#!/bin/sh

cd /sys/class/hidraw || exit
for dev in *; do
   set "$dev"/device/input/*/name
   read name <"$1"
   if [ "$name" = "USB Sound Device" ]; then
      echo "enabling SPDIFMIX on $dev"
      printf "\000\040\001\260\001" >/dev/"$dev"
   fi
done

```

# Disable Pulse/Pipe Audio
```
systemctl --user stop pulseaudio.socket && systemctl --user stop pulseaudio.service
systemctl --user stop pipewire.socket && systemctl --user stop pipewire.service
systemctl --user stop pipewire-pulse.socket && systemctl --user stop pipewire-pulse.service  584  mkdir ~/.config/systemd/user
mkdir -p ~/.config/systemd/user
systemctl --user mask pulseaudio.socket
systemctl --user mask pipewire.socket
systemctl --user mask pipewire-pulse.socket
systemctl --user mask pulseaudio.service
systemctl --user mask pipewire.service
systemctl --user mask pipewire-pulse.service

sudo mkdir /etc/alsa/conf.dis
cd /etc/alsa/conf.d
sudo mv *pulse* ../conf.dis/
sudo mv *pul* ../conf.dis/
sudo mv *pip* ../conf.dis/

sudo cp asound.conf /etc/
```
